<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <meta http-equiv="refresh" content="1">
    </head>
    <%
        String color=request.getParameter("color");
        String [] colors=new String[]{"red", "blue","green"};
        if(color==null){
            color=colors[(int)(Math.random()*3)];
        }
    %>
    <body bgcolor="<%=color%>">
            
        <h1>Hello World!</h1>
    </body>
</html>
